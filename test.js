/**
 * Copyright 2016-2021 the authors (see README.md).
 *
 * This file is part of clean-highlighter.
 *
 * Clean-highlighter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Clean-highlighter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with clean-highlighter. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

const fs = require('fs');
const path = require('path');

const jsdiff = require('diff');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

eval(fs.readFileSync('clean.js').toString());

const GREEN = "\x1b[32m";
const RED   = "\x1b[31m";
const RESET = "\x1b[0m";

function parseTestCase(name, contents) {
	var testcase = {
		name: name,
		type: '',
		entry: 'start',
		test: '',
		result: ''
	};

	var lines = contents.split('\n');
	lines.pop();
	var parse_state = 0;
	for (var i in lines) {
		if (lines[i] == '-----') {
			parse_state++;
			continue;
		}
		switch (parse_state) {
			case 0:
				parts = lines[i].split('=');
				testcase[parts[0]] = parts[1];
				break;
			case 1:
				if (testcase.test != '')
					testcase.test += '\n';
				testcase.test += lines[i];
				break;
			case 2:
				if (testcase.result != '')
					testcase.result += '\n';
				testcase.result += lines[i];
				break;
		}
	}

	if (testcase.type.indexOf(':') != -1) {
		var elems = testcase.type.split(':');
		testcase.type = elems[0];
		testcase.entry = elems[1];
	}

	return testcase;
}

function parseResult(result) {
	var parsedResult = '';
	result = JSDOM.fragment(result);
	for (var i = 0; i < result.children.length; i++) {
		if (parsedResult != '')
			parsedResult += '\n';
		parsedResult +=
			result.children[i].classList.toString().replace(/  /g, ',') + '\t' +
			result.children[i].textContent.replace(/\\/g, '\\\\').replace(/\n/g, '\\n');
	}
	return parsedResult;
}

function resultToHTML(result) {
	const unesc = (c) => {
		switch (c) {
			case 'n':
				return '\n';
			case '\\':
				return '\\';
			default:
				throw 'unexpected character ' + c + ' in unesc';
		}
	};

	var html = '';
	var result = result.split('\n');
	for (var i in result) {
		var elems = result[i].split('\t');
		var cls = elems.shift().replace(/,/g, ' ');
		var content = elems.join('\t').replace(/\\(.)/g, (m, c) => unesc(c));
		html += '<span class="' + cls + '">' + escapeHTML(content) + '</span>';
	}
	return html;
}

var htmlResults;

function runTestCase(testcase) {
	var f = undefined;

	var profile = process.hrtime();
	var result = highlightClean(testcase.test, function(span,cls,str){return span}, testcase.entry);
	var profile = process.hrtime(profile);
	result = parseResult(result);

	var passed = result == testcase.result;

	var time_spent = profile[0] * 1000000 + profile[1] / 1000;
	time_spent = (time_spent / 1000).toFixed(3) + 'ms';

	htmlResults += '<tr class="testcase ' + (passed ? 'passed' : 'failed') + '">';
	htmlResults += '<td>' + testcase.name + '</td>';
	htmlResults += '<td><pre>' + resultToHTML(testcase.result) + '</pre>';
	if (!passed)
		htmlResults += '<pre>' + resultToHTML(result) + '</pre>';
	htmlResults += '</td></tr>';

	process.stderr.write(testcase.name +
			(passed ? GREEN + ' passed' : RED + ' failed') + RESET +
			' ('+ time_spent + ').\n');
	if (!passed) {
		process.exitCode = 1;
		var patch = jsdiff.createPatch(testcase.name + '.result',
				testcase.result, result,
				'Expected result', 'Actual result');
		process.stderr.write('\n  ' + patch.replace(/\n/g, '\n  ') + '\n');
	}

	return passed;
}

fs.readdir('tests', function(err, files) {
	htmlResults = '<!DOCTYPE html>\
		<html>\
		<head>\
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>\
			<link rel="stylesheet" href="clean.css" type="text/css"/>\
			<link rel="stylesheet" href="test_results.css" type="text/css"/>\
			<title>clean-highlighter test results</title>\
		</head>\
		<body>\
			<h1>clean-highlighter test results</h1>\
			<h3 class="{{{RESULTCLASS}}}">{{{RESULTS}}}</h3>\
			<table cellspacing="0">\
				<tr><th>Test case</th><th>Expected and actual result</th></tr>';

	var passed = 0;
	var failed = 0;

	for (var i in files) {
		if (files[i].indexOf('.disabled') != -1)
			continue;
		var contents = fs.readFileSync(path.join('tests', files[i])).toString();
		var testcase = parseTestCase(files[i], contents);
		if (runTestCase(testcase))
			passed++;
		else
			failed++;
	}

	htmlResults = htmlResults.replace('{{{RESULTCLASS}}}', failed > 0 ? 'failed' : 'passed');
	htmlResults = htmlResults.replace('{{{RESULTS}}}', passed + ' passed, ' + failed + ' failed');
	htmlResults += '</table></body></html>';

	fs.writeFileSync('test_results.html', htmlResults);

	if (failed > 0) {
		process.stderr.write('\n' + RED + failed + ' test(s) out of ' + (passed+failed) +
			' failed; see test_results.html for the differences.' + RESET + '\n');
	} else {
		process.stderr.write('\n' + GREEN + 'All is well.' + RESET + '\n');
	}
});
