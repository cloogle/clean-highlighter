# clean-highlighter

A [Clean][] language highlighter. Using state, this highlighter can achieve
much better performance than Clean highlighters in highlight.js, rouge, etc.

This package can be downloaded from [npm][on-npm]:

```bash
npm install clean-highlighter
```

## Authors &amp; license
clean-highlighter is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Mart Lubbers

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

[Camil Staps]: https://camilstaps.nl
[Clean]: http://clean.cs.ru.nl
[on-npm]: https://npmjs.com/package/clean-highlighter
