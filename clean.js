/**
 * Copyright 2016-2021 the authors (see README.md).
 *
 * This file is part of clean-highlighter.
 *
 * Clean-highlighter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Clean-highlighter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with clean-highlighter. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

function highlight(lex, istr, start) {
	var out = [];

	if (!('_prepared' in lex)) {
		for (var group in lex) {
			if ('_prepend' in lex && group[0] != '_')
				lex[group] = lex['_prepend'].concat(lex[group]);
			if ('_append' in lex && group[0] != '_')
				lex[group] = lex[group].concat(lex['_append']);
			for (var i in lex[group]) {
				lex[group][i][0] = new RegExp(
						/^/.source +
						lex[group][i][0].source +
						/([\s\S]*)/.source);
			}
		}
		lex['_prepared'] = true;
	}

	var state_stack = [];
	var state = 'start';
	if (typeof start != 'undefined')
		state = start;

	var equal_classes = function(a, b) {
		if (a == b)
			return a;
		if (a == '__reset__arguments__' && b == 'whitespace')
			return a;
		if (a == 'whitespace' && b == '__reset__arguments__')
			return b;
		return null;
	};

	while (true) {
		var found = false;
		for (var i in lex[state]) {
			var patt = lex[state][i][0];
			var clss = lex[state][i][1];
			var new_states = lex[state][i].length > 2 ? lex[state][i][2] : null;
			if (istr.match(patt)) {
				var parts = patt.exec(istr);
				var j = 0;
				if (typeof clss == 'function') {
					var state_list = state_stack.slice();
					state_list.push(state);
					var result = clss(parts, state_list);
					clss = result[0];
					if (result.length > 1)
						new_states = result[1];
				}
				for (var k in clss) {
					j++;
					if (out.length > 0) {
						var equal = equal_classes(out[out.length-1]['class'], clss[k]);
						if (equal != null) {
							out[out.length-1]['class'] = equal;
							out[out.length-1]['str'] += parts[j];
						} else {
							out.push({'class': clss[k], 'str': parts[j]});
						}
					} else {
						out.push({'class': clss[k], 'str': parts[j]});
					}
				}
				istr = parts[j+1];

				found = true;
				if (new_states != null) {
					new_states = new_states.split(';');
					for (var i in new_states) {
						var new_state = new_states[i];
						if (new_state == 'pop') {
							if (state_stack.length > 0 || i < new_states.length - 1) {
								state = state_stack.pop();
							}
						} else if (new_state.substring(0,4) == 'pop:') {
							for (var p = 0; p < parseInt(new_state.substring(4)); p++)
								state = state_stack.pop();
						} else if (new_state.substring(0,5) == 'jump:') {
							state = new_state.substring(5);
						} else if (new_state.substring(0,6) == 'reset:') {
							state = new_state.substring(6);
							state_stack = [];
						} else {
							state_stack.push(state);
							state = new_state;
						}
					}
				}

				break;
			}
		}
		if (!found && istr != '')
			console.log('clean.js: no rule applicable in state ' + state + ' for "' + istr + '"');
		if (!found || istr == '')
			return out;
	}
}

function highlightToHTML(lex, istr, callback, start) {
	var elems = highlight(lex, istr, start);
	var ostr = '';
	for (var i in elems) {
		var cls = elems[i]['class'];
		var str = elems[i]['str'];
		var span = '<span class="' + cls + '">' + escapeHTML(str) + '</span>';
		if (typeof callback == 'function') {
			ostr += callback(span, cls, str);
		} else {
			ostr += span;
		}
	}
	return ostr;
}

var paren_depth, indents;
var highlightRules = {
	_prepend: [
		[/(\s*)(\/\/.*)/, ['whitespace', 'comment']],
		[/(\s*)(\/\*)/,   ['whitespace', 'comment'], 'comment'],
		[/(')([\w`]+)('\.)/, ['punctuation', 'qualified', 'punctuation']],
	],
	_append: [
		[/(\n\s*(?=\n))/, ['whitespace']],
		[/(\n[^\n\S]*)/, function(matches, states) {
				var whitespace = matches[1];
				var rest = matches[2];
				var indent = whitespace.substring(1).replace(/\t/g,'    ').length;

				var cls = 'whitespace';

				if (indent == 0) {
					indents = [[0, 'start']];
					if (!rest.match(/^[\s\n]*where\b/))
						cls = '__reset__arguments__';
					else if (rest.match(/^where\b\s*\S/)) {
						var next_indent = rest.match(/^(where\b\s*)\S/)[1].replace(/\t/g,'    ').length;
						indents.unshift([indent+next_indent, 'start']);
					}
					return [[cls], 'reset:start'];
				}

				var tried_reset_arguments = false;
				while (1) {
					if (indent > indents[0][0]) {
						if (rest.match(/^[\s\n]*where\b/)) {
							if (rest.match(/^where\s*\S/)) {
								var next_indent = rest.match(/^(where\b\s*)\S/)[1].replace(/\t/g,'    ').length;
								indents.unshift([indent+next_indent, 'start']);
							} else {
								indents.unshift([indent, 'start']);
							}
						} else {
							indents.unshift([indent, states.join(';')]);
						}
						return [['whitespace']];
					}

					if (indent == indents[0][0])
						return [[cls], 'reset:' + indents[0][1]];

					indents.shift();

					if (!tried_reset_arguments) {
						if (!rest.match(/^[\s\n]*where\b/))
							cls = '__reset__arguments__';
						tried_reset_arguments = true;
					}
				}
			}],
		[/([^\n\S]+)/,   ['whitespace']],
		[/(.)/,          ['error']]
	],
	comment: [
		[/([^*\\]+)/,    ['comment']],
		[/(\*\/)/,       ['comment'], 'pop'],
		[/(\/\*)/,       ['comment'], 'comment'],
		[/(\*|\\)/,      ['comment']]
	],
	start: [
		[/(generic)(\s)/,
		                 ['keyword', 'whitespace'], 'generic'],
		[/(import)(\s)/, ['keyword', 'whitespace'], 'import'],
		[/(from)(\s)/,   ['keyword', 'whitespace'], 'importFrom'],
		[/(where\b)/,    ['keyword']],
		[/((?:(?:definition|implementation|system)\s+)?module\b)/,
		                 ['keyword'], 'moduleName'],
		[/(instance)(\s)/, ['keyword', 'whitespace'], 'instance'],
		[/(derive)(\s+)(class)/, ['keyword', 'whitespace', 'keyword'], 'deriveClass'],
		[/(derive)(\s)/, ['keyword', 'whitespace'], 'derive'],
		[/(class)(\s)/,  ['keyword', 'whitespace'], 'className'],
		[/(::)/,         ['punctuation'], 'typeDefName'],
		[/([a-zA-Z][\w`]*)(\s*)(::)/,
		                 ['funcname', 'whitespace', 'punctuation'], 'type'],
		[/([~@#$%^?!:+*<>\\\/|&=-]+)(\s+)(::)/,
		                 ['funcname', 'whitespace', 'punctuation'], 'type'],
		[/(\()(\S+)(\))(\s*)(infix[rl]?)(\s*)(\d*)(\s*)(::)/,
		                 ['punctuation', 'funcname', 'punctuation', 'whitespace',
		                  'keyword', 'whitespace', 'keyword', 'whitespace',
		                  'punctuation']
		                 , 'type'],
		[/(\()(\S+)(\))(\s+)(infix[rl]?)(\s*)(\d*)/,
		                 ['punctuation', 'funcname', 'punctuation', 'whitespace',
		                  'keyword', 'whitespace', 'keyword']],
		[/(\()(\S+)(\))/,['punctuation', 'funcname', 'punctuation'], 'funcargs'],
		[/(infix[rl]?)(\s+)(\d*)/,
		                 ['keyword', 'whitespace', 'keyword']],
		[/(foreign export(?: (?:c|std)call)?)/,
		                 ['keyword']],
		[/([\(\)])/,     ['punctuation']],
		[/(::)/,         ['punctuation'], 'type'],
		[/({)(\s*)(32)(\s*)(#)/,
		                 ['punctuation','whitespace','punctuation','whitespace','punctuation']],
		[/([{};])/,      ['punctuation']],
		[/(=>)/,         ['punctuation']],
		[/(")/,          ['literal literal-string'], '_string'],
		[/(\|)/,         ['punctuation'], 'guard'],
		[/(#)/,          ['punctuation'], 'funcargs'],
		[/(=)/,          ['punctuation'], 'rhs'],
		[/([^\(\){\s]+)({\|)/, ['funcname', 'punctuation'], 'genericArgs'],
		[/([^\(\){[\s]+)/, ['funcname'], 'funcargs']
	],
	moduleName: [
		[/([^\s;]+)(\s+)(with)(\s+)/, ['modulename', 'whitespace', 'keyword', 'whitespace'], 'jump:moduleSettings'],
		[/([^\s;]+)/,    ['modulename'], 'pop']
	],
	moduleSettings: [
		[/(\[!?\])/,     ['punctuation'], 'pop']
	],
	funcargs: [
		[/(::)/,         ['punctuation'], 'type'],
		[/(=:)/,         ['punctuation']],
		[/((?::=)?=)/,   ['punctuation'], 'jump:rhs'],
		[/(->)/,         ['punctuation'], 'jump:rhs'], /* funcargs is also used for cases */
		[/(\[\|)/,       ['punctuation']], /* overloaded list */
		[/([:\[\](),{}=])/, ['punctuation']],
		[/(\|})/,        ['punctuation']],
		[/(")/,          ['literal literal-string'], '_string'],
		[/(\|)/,         ['punctuation'], 'jump:guard'],
		[/(#)/,          ['punctuation']],
		[/([A-Z][^:\[\](),{}=\s]*)/, ['constructor']],
		[/(\?[#^|]?(?:None|Just))/, ['constructor']],
		[/([^:\[\](),{}=|\s]+)/, ['argument']]
	],
	startConstructor: [ // alternative entry point in case this is a constructor
		[/(\()(.*)(\))(\s*)(infix[lr]?)(\s*)(\d*)(\s*)(::)/,
		                 ['punctuation', 'constructor', 'punctuation',
		                  'whitespace', 'keyword', 'whitespace', 'keyword',
		                  'whitespace', 'punctuation'], 'type'],
		[/(.*)(::)/,     ['constructor', 'punctuation'], 'type']
	],
	startRecordField: [ // alternative entry point in case this is a record field
		[/(.*)(::)/,     ['field', 'punctuation'], 'type']
	],
	generic: [
		[/(\S+)/,        ['funcname funcname-generic'], 'jump:genericVars']
	],
	genericVars: [
		[/(::)/,         ['punctuation'], 'jump:type'],
		[/(\|)/,         ['punctuation'], 'genericContext'],
		[/(\S+)/,        ['typevar']]
	],
	genericContext: [
		[/(\S+)/,        ['generic'], 'pop']
	],
	genericArgs: [
		[/\b(of)\b/,     ['keyword'], 'jump:funcargs'],
		[/([A-Z]\w*)/,   ['type']],
		[/(\()(->)(\))/, ['punctuation', 'type', 'punctuation']],
		[/([[\]{}!#()>, -]+)/, ['type']],
		[/([a-z]\w*)/,   ['typevar']],
		[/(\|})/,        ['punctuation'], 'jump:funcargs'],
	],
	type: [
		[/(where)/,      ['keyword'], 'pop'],
		[/(:==)(\s*)(code)(\s*)({)/,
		                 ['punctuation', 'whitespace', 'keyword', 'whitespace', 'punctuation'], 'jump:abc'],
		[/\b(of)\b/,     ['keyword'], 'typeOfPattern'],
		[/([a-z][\w`]*)/, ['typevar']],
		[/(A)(\.)/,      ['existential', 'punctuation'], 'quantifiedVariables'],
		[/(E)(\.)(\s*)(\^)/, ['existential', 'punctuation', 'whitespace', 'punctuation'], 'quantifiedVariables'],
		[/([A-Z_][\w`]*)/, ['type']],
		[/(\(\))/,       ['type']],
		[/(\()(->)(\))/, ['punctuation', 'type', 'punctuation']],
		[/(\|)/,         ['punctuation'], 'context'],
		[/(\()/,         ['punctuation'], 'type'],
		[/(\))/,         ['punctuation'], 'pop'],
		[/({\s*32\s*#)/, ['punctuation']],
		[/([^\s\w])/,    ['punctuation']]
	],
	typeOfPattern: [
		[/([a-z]\w*)/,   ['argument']],
		[/([A-Z]\w*)/,   ['constructor']],
		[/(\?[#^|]?(?:None|Just))/, ['constructor']],
		[/([{([])/,      ['punctuation'], 'typeOfPattern'],
		[/([})\]])/,     ['punctuation'], 'pop'],
		[/([=:]+)/,      ['punctuation']],
		[/(,)/,          function (matches, states) {
			if (states[states.length-2] == 'typeOfPattern')
				return [['punctuation']];
			else
				return [['punctuation'], 'pop'];
		}]
	],
	quantifiedVariables: [
		[/(\*)/,         ['punctuation']],
		[/(:)/,          ['punctuation'], 'pop'],
		[/([a-z][\w`]*)/, ['typevar']]
	],
	context: [
		[/(,)/,          ['punctuation']],
		[/(\[)/,         ['punctuation'], 'attrenv'],
		[/(\S+)(\{\|)/,  ['generic', 'punctuation'], 'contextKind'],
		// These are two hacks for class context in universally quantified types:
		[/(\()/,         ['punctuation'], 'pop'], // hack for sqlShare
		[/(->)/,         ['punctuation'], 'pop'], // hack for sqlExecute
		// End hacks
		[/([^\s{]+)(,)/, ['classname', 'punctuation']],
		[/([^\s{]+)/,    ['classname'], 'contextType']
	],
	contextKind: [
		[/([*>-]+\|\},)/, ['punctuation'], 'pop'],
		[/([*>-]+\|\})/, ['punctuation'], 'jump:contextType']
	],
	contextType: [
		[/(special)/,    ['keyword'], 'pop:2;special'],
		[/(where)/,      ['keyword'], 'pop:3'],
		[/(::)/,         ['punctuation'], 'pop:2'],
		[/([,;])/,       ['punctuation']],
		[/([&\|])/,      ['punctuation'], 'pop'],
		[/(\[)/,         ['punctuation'], 'attrenv'],
		[/([\(\[])/,     ['punctuation'], 'contextType'],
		[/([\)\]])/,     ['punctuation'], 'pop'],
		[/([a-z][\w`]*)/, ['typevar']],
		[/([A-Z_][\w`]*)/, ['type']],
		[/([^\s\w])/,    ['punctuation']],
		[/([^\s\(\)\[\],]+)/, ['typevar']]
	],
	attrenv: [
		[/(\w)/,         ['typevar']],
		[/(<=)/,         ['punctuation']],
		[/(,)/,          ['punctuation']],
		[/(\])/,         ['punctuation'], 'pop']
	],
	macro: [
		[/(\(.+\)\s+infix.*)/,
		                 ['__type__']],
		[/([\w`]+\s*::.*)/,
		                 ['__type__']],
		[/\b(where|with)\b/,  ['keyword'], 'jump:macro'],
		[/(special)(\s)/,['keyword', 'whitespace'], 'special'],
		[/(\()(\S+)(\))/, ['punctuation', 'funcname', 'punctuation'], 'funcargs'],
		[/(\S+)/,        ['funcname'], 'funcargs']
	],
	guard: [
		[/((?:^|\s)= )/, ['punctuation'], 'jump:rhs'],
		[/(otherwise)/,  ['keyword']],
		[/('(?:[^'\\]|\\(?:x[0-9a-fA-F]+|\d+|.))')/,
		                 ['literal literal-char']],
		[/([+~-]?\d+\.\d+(?:E[+-]?\d+)?)\b/,
		                 ['literal literal-real']],
		[/([+~-]?\d+E[+-]?\d+)\b/,
		                 ['literal literal-real']],
		[/([+~-]?0[0-7]+)\b/,
		                 ['literal literal-int literal-int-oct']],
		[/([+~-]?\d+)\b/,['literal literal-int literal-int-dec']],
		[/([+~-]?0x[\da-fA-F]+)\b/,
		                 ['literal literal-int literal-int-hex']],
		[/\b(True|False)\b/,
		                 ['literal literal-bool']],
		[/(")/,          ['literal literal-string'], '_string'],
		[/(\[)(\s*)(')(?=.*'\])/,
		                 ['punctuation', 'whitespace', 'literal literal-char'], '_charlist'],
		[/([A-Z][\w`]*)/,['constructor']],
		[/(\?[#^|]?(?:None|Just)?)/, ['constructor']],
		[/\b(_)\b/,      ['argument argument-wildcard']],
		[/([\w`]+)/,     ['funcname funcname-onlyused']],
		[/(\S)/,         ['punctuation']]
	],
	rhs: [
		[/(\n)(\|)/,     ['whitespace', 'punctuation'], 'jump:guard'],
		[/(code\s+(?:inline\s*)?)({)/,
		                 ['keyword', 'punctuation'], 'abc'],
		[/\b(if|let|in|with|case|otherwise)\b/,
		                 ['keyword']],
		[/\b(of)\b/,     ['keyword'], 'funcargs'],
		[/('[\w`]+'\.)/, ['qualifiedname']],
		[/('(?:[^'\\]|\\(?:x[0-9a-fA-F]+|\d+|.))')/,
		                 ['literal literal-char']],
		[/\b([+~-]?\d+\.\d+(?:E[+-]?\d+)?)\b/,
		                 ['literal literal-real']],
		[/([+~-]?\d+E[+-]?\d+)\b/,
		                 ['literal literal-real']],
		[/\b([+~-]?0[0-7]+)\b/,
		                 ['literal literal-int literal-int-oct']],
		[/\b([+~-]?\d+)\b/,
		                 ['literal literal-int literal-int-dec']],
		[/\b([+~-]?0x[\da-fA-F]+)\b/,
		                 ['literal literal-int literal-int-hex']],
		[/\b(True|False)\b/,
		                 ['literal literal-bool']],
		[/(")/,          ['literal literal-string'], '_string'],
		[/(\[)(\s*)(')(?=.*'\])/,
		                 ['punctuation', 'whitespace', 'literal literal-char'], '_charlist'],
		[/(\(.+\)\s+infix.*)/,
		                 ['__type__']],
		[/([\w`]+(?:\s*[^\S\n])?::.*)/,
		                 ['__type__']],
		[/([A-Z][\w`]*)/,['constructor']],
		[/(\?[#^|]?(?:None|Just))/, ['constructor']],
		[/\b(_)\b/,      ['argument argument-wildcard']],
		[/\b(where)\b/,  ['keyword'], 'jump:start'],
		[/([\w`]+)/,     ['funcname funcname-onlyused']],
		[/(\S)/,         ['punctuation']]
	],
	special: [
		[/([a-z][\w`]*)/,['typevar']],
		[/(=)/,          ['punctuation'], 'type']
	],
	_string: [
		[/(")/,          ['literal literal-string'], 'pop'],
		[/(\\.)/,        ['literal literal-string']],
		[/([^\\"]+)/,    ['literal literal-string']]
	],
	_charlist: [
		[/(')(\s*)(\])/, ['literal literal-char', 'whitespace', 'punctuation'], 'pop'],
		[/(')/,          ['literal literal-char'], 'pop'],
		[/(\\.)/,        ['literal literal-char']],
		[/([^\\']+)/,    ['literal literal-char']]
	],
	instance: [
		[/(\S+)/,        ['classname'], 'jump:type'],
	],
	className: [
		[/(\()(.*)(\))(\s*)(infix[lr]?)(\s*)(\d*)(\s*)/,
		                 ['punctuation', 'classname', 'punctuation', 'whitespace',
		                  'keyword', 'whitespace', 'keyword', 'whitespace'],
		                 'jump:classHeading'],
		[/(\S+)/,        ['classname'], 'jump:classHeading']
	],
	classHeading: [
		[/(\n)/,         ['whitespace'], 'pop'],
		[/(where)/,      ['keyword'], 'pop'],
		[/([a-z][\w`]*)/, ['typevar']],
		[/([.~()])/,     ['punctuation']],
		[/(\|)/,         ['punctuation'], 'jump:classContext'],
		[/(::)/,         ['punctuation'], 'jump:type']
	],
	classContext: [
		[/(where)/,      ['keyword'], 'pop'],
		[/(,)/,          ['punctuation']],
		[/(\S+)(\{\|)/,  ['generic', 'punctuation'], 'classGeneric'],
		[/([^\s{]+)(,)/, ['classname', 'punctuation']],
		[/([^\s{]+)/,    ['classname'], 'classContextType']
	],
	classGeneric: [
		[/([*>-]+\|\},)/, ['punctuation'], 'pop'],
		[/([*>-]+\|\})/, ['punctuation'], 'classContextType']
	],
	classContextType: [
		[/(where)/,      ['keyword'], 'pop:2'],
		[/([,&])/,       ['punctuation'], 'pop'],
		[/([^\s,]+)/,    ['typevar']]
	],
	derive: [
		[/(\S+)/,        ['generic'], 'jump:type'],
	],
	deriveClass: [
		[/(\S+)(\s*)(\\)(\s*)/,
		                 ['classname', 'whitespace', 'punctuation', 'whitespace'],
		                 'jump:deriveClassExcept'],
		[/(\S+)/,        ['classname'], 'jump:type'],
	],
	deriveClassExcept: [
		[/(\S+)(\s*)(,)(\s*)/,
		                 ['generic', 'whitespace', 'punctuation', 'whitespace']],
		[/(\S+)/,        ['generic'], 'jump:type'],
	],
	abc: [
		[/(\.)/,         ['abc-directive'], 'abcDirective'],
		[/(\s+)/,        ['whitespace'], 'abcInstruction'],
		[/(\|.*\n)/,     ['comment']],
		[/(})/,          ['punctuation'], 'pop'],
		[/([^\s}]+)/,    ['label'], 'abcInstruction']
	],
	abcInstruction: [
		[/(})/,          ['punctuation'], 'pop:2'],
		[/(;)/,          ['punctuation'], 'pop'],
		[/(\n)/,         ['whitespace'], 'pop'],
		[/(\|.*\n)/,     ['comment'], 'pop'],
		[/([^\s}]+)/,    ['abc-instruction'], 'jump:abcArgument']
	],
	abcArgument: [
		[/(\n)/,         ['whitespace'], 'pop'],
		[/(})/,          ['punctuation'], 'pop:2'],
		[/(;)/,          ['punctuation'], 'pop'],
		[/(\|.*\n)/,     ['comment'], 'pop'],
		[/('(?:[^'\\]|\\(?:x[0-9a-fA-F]+|\d+|.))')/,
		                 ['literal literal-char']],
		[/([+~-]?\d+\.\d+(?:E[+-]?\d+)?)\b/,
		                 ['literal literal-real']],
		[/([+~-]?\d+E[+-]?\d+)\b/,
		                 ['literal literal-real']],
		[/([+~-]?\d+)\b/,['literal literal-int literal-int-dec']],
		[/([+~-]?0x[\da-fA-F]+)\b/,
		                 ['literal literal-int literal-int-hex']],
		[/\b(TRUE|FALSE)\b/,
		                 ['literal literal-bool']],
		[/(")/,          ['literal literal-string'], '_string'],
		[/([^\s}]+)/,    ['abc-argument']]
	],
	abcDirective: [
		[/(\n)/,         ['whitespace'], 'pop'],
		[/(})/,          ['punctuation'], 'pop:2'],
		[/(;)/,          ['punctuation'], 'pop'],
		[/(\|.*\n)/,     ['comment'], 'pop'],
		[/(\S+)/,        ['abc-directive'], 'jump:abcArgument']
	],
	typeDefName: [
		[/(\*)/,         ['punctuation']],
		[/(\(?:==)/,     ['punctuation'], 'jump:typeDefSynonym'],
		[/(\(?=:?|\|)/,  ['punctuation'], 'jump:typeDefRhs'],
		[/([A-Z_][\w`]*)/, ['type']],
		[/(\[! \])/,     ['type']], // Special cases for head-strict and tail-strict lists;
		[/(\[ !\])/,     ['type']], // we don't want to parse whitespace there
		[/(\()(->)(\))/, ['punctuation', 'type', 'punctuation']],
		[/({32#})/,      ['type']],
		[/([~@#\$%\^\?!\+\-\*<>\\\/\|&=:\[\]{}(,)]+)/, ['type']],
		[/([a-z_`][\w`]*)/, ['typevar'], 'jump:typeDefVars']
	],
	typeDefVars: [
		[/([a-z][\w`]*)/, ['typevar']],
		[/(\(?:==)/,     ['punctuation'], 'jump:typeDefSynonym'],
		[/(\(?=:?|\|)/,  ['punctuation'], 'jump:typeDefRhs'],
	],
	typeDefSynonym: [
		[/([a-z][a-zA-Z]*)/, ['typevar']],
		[/([A-Z_][\w`]*)/, ['type']],
		[/({\s*32\s*#)/, ['punctuation']],
		[/([^\w`\s])/,   ['punctuation']]
	],
	typeDefRhs: [
		[/(\s*)(E)(\.)/, ['whitespace', 'existential', 'punctuation'], 'jump:typeDefRhsExi'],
		[/(\s*)(\{)/,    ['whitespace', 'punctuation'], 'jump:typeDefRecord'],
		[/(\s*)(!)(\s*)(\{)/,
		                 ['whitespace', 'punctuation', 'whitespace', 'punctuation'], 'jump:typeDefRecord'],
		[/(\s*)/,        ['whitespace'], 'jump:typeDefADT']
	],
	typeDefRhsExi: [
		[/([a-z][\w`]*)/, ['typevar']],
		[/(:)/,          ['punctuation'], 'jump:typeDefRhs']
	],
	typeDefRecord: [
		[/([a-z_][\w`]*)(\s*)(::)/,
		                 ['field', 'whitespace', 'punctuation'], 'typeDefFieldType'],
		[/(\})/,         ['punctuation']]
	],
	typeDefFieldType: [
		[/([a-z][\w`]*)/, ['typevar']],
		[/([A-Z_][\w`]*)/, ['type']],
		[/(\()/,         ['punctuation'], 'typeDefTuple'],
		[/([\[\{])/,     ['punctuation'], 'typeDefFieldType'],
		[/([\]\},])/,    ['punctuation'], 'pop'],
		[/([^\w\s])/,    ['punctuation']]
	],
	typeDefTuple: [
		[/([a-z][\w`]*)/, ['typevar']],
		[/([A-Z_][\w`]*)/, ['type']],
		[/([\(\[\{])/,   ['punctuation'], 'typeDefTuple'],
		[/([\)\]\}])/,   ['punctuation'], 'pop'],
		[/([^\w\s])/,    ['punctuation']]
	],
	typeDefADT: [
		[/(E)(\.)/,      ['existential', 'punctuation'], 'quantifiedVariables'],
		[/([A-Z_][\w`]*)/, ['constructor'], 'typeDefConsArgs'],
		[/(\?[#^|]?(?:None|Just))/, ['constructor'], 'typeDefConsArgs'],
		[/(\()([~@#\$%\^\?!\+\-\*<>\\\/\|&=:]*)(\))/,
		                 ['punctuation', 'constructor', 'punctuation'],
		                 'typeDefConsArgs'],
		[/(\.\.)/,       ['punctuation']]
	],
	typeDefConsArgs: [
		[/(infix[lr]?)(\s*)(\d*)(\s+)/,
		                 ['keyword', 'whitespace', 'keyword', 'whitespace']],
		[/([a-z][\w`]*)/, ['typevar']],
		[/(A)(\.)/,      ['existential', 'punctuation'], 'quantifiedVariables'],
		[/([A-Z][\w`]*)/, ['type']],
		[/(\|)/,         ['punctuation'], 'pop'],
		[/(&)/,          ['punctuation'], 'jump:typeDefContext'],
		[/([^\w\s])/,    ['punctuation']]
	],
	typeDefContext: [
		[/(,)/,          ['punctuation']],
		[/(\S+)(\{\|)/,  ['generic', 'punctuation'], 'typeDefContextGeneric'],
		[/([^\s{]+)(,)/, ['classname', 'punctuation']],
		[/([^\s{]+)/,    ['classname'], 'typeDefContextType']
	],
	typeDefContextGeneric: [
		[/([*>-]+\|\},)/, ['punctuation'], 'pop'],
		[/([*>-]+\|\})/, ['punctuation'], 'jump:typeDefContextType']
	],
	typeDefContextType: [
		[/(&)/,          ['punctuation'], 'pop'],
		[/(\|)/,         ['punctuation'], 'pop:2'],
		[/([^\s,]+)/,    ['typevar']]
	],
	import: [
		[/(,)/,          ['punctuation']],
		[/(=>)(\s+)(qualified\b)/, ['punctuation', 'whitespace', 'keyword'], 'importSelections'],
		[/(qualified\b)/, ['keyword']],
		[/(as\b)/,       ['keyword'], 'importAs'],
		[/(code\s+from(?:\s+library)?\s+)(")/, ['keyword', 'literal literal-string'], '_string'],
		[/([^,\s]+)/,    ['modulename']]
	],
	importAs: [
		[/(,)/,          ['punctuation'], 'pop'],
		[/([^,\s]+)/,    ['qualified']]
	],
	importFrom: [
		[/([^,\s]+)(\s+)(import)/, ['modulename', 'whitespace', 'keyword'], 'importSelections']
	],
	importSelections: [
		[/(,)([\s\n]*)/, ['punctuation', 'whitespace']],
		[/(qualified\b)/,['keyword']],
		[/(class\b)/,    ['keyword'], 'importSelectionsClass'],
		[/(instance\b)/, ['keyword'], 'importSelectionsInstance'],
		[/(generic\b)/,  ['keyword']],
		[/(::)/,         ['punctuation'], 'importSelectionsTypeDef'],
		[/([^\s,]+)/,    ['funcname']]
	],
	importSelectionsClass: [
		[/(,)([\s\n]*)/, ['punctuation', 'whitespace'], 'pop'],
		[/(\()/,         ['punctuation'], 'importSelectionsClassMembers'],
		[/([^\s,(]+)/,   ['classname']]
	],
	importSelectionsClassMembers: [
		[/(\))/,         ['punctuation'], 'pop'],
		[/(,)/,          ['punctuation']],
		[/(\.\.)/,       ['punctuation']],
		[/([^\s,)]+)/,   ['funcname']]
	],
	importSelectionsInstance: [
		[/(\S+)/,        ['classname'], 'jump:importSelectionsInstanceType']
	],
	importSelectionsInstanceType: [
		[/(,)/,          function (m) { return paren_depth == 0 ? [['punctuation'], 'pop'] : [['punctuation']]; }],
		[/([\(\[{])/,    function (m) { paren_depth++; return [['punctuation'], 'importSelectionsInstanceType']; }],
		[/([\)\]}])/,    function (m) { paren_depth--; return [['punctuation'], 'pop']; }],
		[/([a-z][\w`]*)/, ['typevar']],
		[/([A-Z_][\w`]*)/, ['type']],
		[/([^\s\(\)\[\]{}#!,]+)/, ['typevar']],
		[/([#!]+)/,      ['punctuation']]
	],
	importSelectionsTypeDef: [
		[/(,)([\s\n]*)/, ['punctuation', 'whitespace'], 'pop'],
		[/([^\s({,]+)/,  ['type']],
		[/(\()/,         ['punctuation'], 'importSelectionsADTConses'],
		[/(\{)/,         ['punctuation'], 'importSelectionsRecordFields'],
	],
	importSelectionsADTConses: [
		[/(\))/,         ['punctuation'], 'pop'],
		[/(,)/,          ['punctuation']],
		[/(\.\.)/,       ['punctuation']],
		[/([^\s,)]+)/,   ['constructor']]
	],
	importSelectionsRecordFields: [
		[/(\})/,         ['punctuation'], 'pop'],
		[/(,)/,          ['punctuation']],
		[/(\.\.)/,       ['punctuation']],
		[/([^\s},]+)/,   ['field']]
	]
};

function highlightClean(func, callback, start) {
	var macroargs = [];
	paren_depth = 0;
	indents = [[0, 'start']];

	var myCallback = function(span, cls, str) {
		if (cls == '__type__') {
			return highlightClean(str, callback);
		} else if (cls == '__reset__arguments__') {
			macroargs = [];
			span = span.replace('class="' + cls + '"', 'class="whitespace"');
			cls = 'whitespace';
		} else if (cls == 'argument') {
			macroargs.push(str);
		} else if (macroargs.indexOf(str) >= 0) {
			span = span.replace('class="' + cls + '"', 'class="argumentinrhs"');
			cls = 'argumentinrhs';
		}

		if (typeof callback == 'function')
			return callback(span, cls, str);
		else
			return span;
	};

	return highlightToHTML(highlightRules, func, myCallback, start);
}

function highlightType(type, callback) {
	return highlightClean(type, callback, 'type');
}

function escapeHTML(unsafe) {
	var map = { '&': '&amp;', '<': '&lt;', '>': '&gt;',
		'"': '&quot;', "'": '&#39;', "/": '&#x2F;' };
	return String(unsafe).replace(/[&<>"'\/]/g, function(s){return map[s];});
}
